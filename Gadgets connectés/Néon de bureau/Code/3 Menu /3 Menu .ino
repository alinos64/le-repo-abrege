#include <Arduino.h>
#include <Servo.h>
#include "FastLED.h"

Servo servo;
CRGB leds[42];
int MENU = 0;

void workstate();
void chillstate();
void off();

void setup() {
  Serial.begin(9600);
  servo.attach(D2);
  pinMode(D7, INPUT_PULLUP);
  FastLED.setMaxPowerInVoltsAndMilliamps(5, 600);
  FastLED.addLeds<NEOPIXEL, D5>(leds, 42);

}

void loop() {           
  int sensor = digitalRead(D7);
  Serial.println(MENU);

  if (sensor == LOW){
    MENU = MENU + 1;
    if (MENU == 3){
      MENU = 0;
    }
  }

  if (MENU == 0){
    chillstate();
    Serial.println("MENU : Chillstate");

  }  
  
  if (MENU == 1) {
    workstate();
    Serial.println("MENU : Workstate");
  }

  if (MENU == 2) {
    off();
    Serial.println("MENU : Off");
 

  }
   delay(300);

}

void workstate(){
  servo.write(180);
  fill_solid(leds, 42, CRGB::WhiteSmoke);
  FastLED.setBrightness(120);
  FastLED.show();
}

void chillstate(){
  servo.write(0);
  fill_solid(leds, 42, CRGB::BlueViolet);
  FastLED.setBrightness(60);
  FastLED.show();
}

void off(){
  servo.write(80);
  fill_solid(leds, 42, CRGB::Black);
  FastLED.show();
}